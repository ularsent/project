# импортируем модуль для работы и связываемся с созданным ботом
import random
import telebot
from telebot import types
bot = telebot.TeleBot('6151953611:AAHB0RAJ9G-Fz7a1wXql-HUGyjmoY5YqfL0')


# самый старт
@bot.message_handler(commands=["start"])
def start(message):
    global user_id, streak, max_streak
    user_id = message.from_user.id
    print(user_id)
    streak[user_id] = 0  # серия ответов (с привязкой к пользователю)
    max_streak[user_id] = 0  # рекорд пользователя
    restart(message)


mode = {}


# рестарт (переход в выбор режима)
def restart(message):
    global mode
    mode[user_id] = 'choice'

    hi_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    hi_markup.add(types.KeyboardButton('гласные'), types.KeyboardButton('согласные'),
                  types.KeyboardButton('спектрограммы'), types.KeyboardButton('символы — описания'),
                  types.KeyboardButton('символы — аудио'))
    hi_text = 'Привет, друг! Поиграем в фонетические игры? (:\n\nВыбери режим:\n\n' \
              '*гласные*: бот присылает аудио с каким-то гласным и пять описаний, ' \
              'из которых нужно выбрать верное.\n\n' \
              '*согласные*: бот присылает аудио с каким-то согласным и пять описаний, ' \
              'из которых вам нужно выбрать верное.\n\n' \
              '*спектрограммы*: бот присылает спектрограмму произнесения какого-то животного по-английски, ' \
              'нужно написать животное самим\n\n' \
              '*символы — описания*: бот присылает символ МФА и пять описаний звуков, ' \
              'вам нужно выбрать, какому из них соответствует символ\n\n' \
              '*символы — аудио*: бот присылает символ МФА и пять аудиозаписей звуков, ' \
              'нужно выбрать, какому из них соответствует символ'
    bot.send_message(message.chat.id, hi_text, reply_markup=hi_markup, parse_mode="Markdown")


streak = {}
max_streak = {}


# реакция на выбор режима
@bot.message_handler(func=lambda message: message.text in ['согласные', 'гласные', 'спектрограммы',
                                                           'символы — описания', 'символы — аудио', 'гласные'])
def mode_choice(message):

    if message.text == 'согласные':
        consonants_mode(message)

    elif message.text == 'спектрограммы':
        spectro_mode(message)

    elif message.text == 'символы — описания':
        symbols_desc_mode(message)

    elif message.text == 'символы — аудио':
        symbols_audio_mode(message)

    elif message.text == 'гласные':
        vowels_mode(message)


ipa_consonants = {
    'wxfgtrebvngyz.mp3': ['альвеолярный аппроксимант', 'ɹ'],
    'wxrtsdkiouyz.mp3': ['альвеолярный латеральный аппроксимант', 'l'],
    'wxtyuippjhcyz.mp3': ['лабиодентальный аппроксимант', 'ʋ'],
    'wxdfgtreiougyz.mp3': ['палатальный аппроксимант', 'j'],
    'wxdfgtrmnbghyz.mp3': ['палатальный латеральный аппроксимант', 'ʎ'],
    'wxmnbuytpolyz.mp3': ['ретрофлексный аппроксимант', 'ɻ'],
    'wxdmniuyasdyz.mp3': ['ретрофлексный латеральный аппроксимант', 'ɭ'],
    'wxoihbvfrtyz.mp3': ['велярный латеральный аппроксимант', 'ʟ'],
    'wxaslkjuyrtoyz.mp3': ['звонкий альвеолярный латеральный щелевой', 'ɮ'],
    'wxertvcdftiyz.mp3': ['велярный аппроксимант', 'ɰ'],
    'wxiuytbnhpyz.mp3': ['глухой альвеолярный латеральный щелевой', 'ɬ'],
    'wxdfnbvmoiyz.mp3': ['глухой палатальный щелевой', 'ç'],
    'wxuytvbcdfyz.mp3': ['звонкий глоттальный щелевой', 'ɦ'],
    'wxyutrecvklyz.mp3': ['звонкий палатальный щелевойй', 'ʝ'],
    'wxytrbvgftryz.mp3': ['звонкий фаренгальный щелевой', 'ʕ'],
    'wxytrvbncmjyz.mp3': ['звонкий ретрофлексный сибилянт', 'ʐ'],
    'wxdrewuytryz.mp3': ['звонкий увулярный щелевой', 'ʁ'],
    'wxfdretejcpoyz.mp3': ['звонкий велярный щелевой', 'ɣ'],
    'wxdretyrupoiyz.mp3': ['глухой глоттальный щелевой', 'h'],
    'wxdreytbvfgyz.mp3': ['глухой фаренгальный щелевой', 'ħ'],
    'wxretrgfvbcyz.mp3': ['глухой ретрофлексный сибилянт', 'ʂ'],
    'wxdretgfbvcyz.mp3': ['глухой увулярный щелевой', 'χ'],
    'wxfretvclpozyz.mp3': ['глухой велярный щелевой', 'х'],
    'wxtrgfhjpoiyz.mp3': ['звонкий билабиальный щелевой', 'β'],
    'wxdfremlpofyz.mp3': ['звонкий дентальный щелевой', 'ð'],
    'wxhgmnopiuyz.mp3': ['звонкий лабио-дентальный щелевой', 'v'],
    'wxfdlpoibvyz.mp3': ['глухой лабио-дентальный щелевой', 'f'],
    'wxhgflopimnyz.mp3': ['звонкий альвеолярный сибилянт', 'z'],
    'wxtrenbvopyz.mp3': ['звонкий постальвеолярный сибилянт', 'ʒ'],
    'wxhguoipoyz.mp3': ['глухой альвеолярный сибилянт', 's'],
    'wxuytbvdfgyz.mp3': ['глухой билабиальный щелевой', 'ɸ'],
    'wxpolkmnayz.mp3': ['глухой дентальный щелевой', 'θ'],
    'wxfdtrybvfgyz.mp3': ['глухой постальвеолярный сибилянт', 'ʃ'],
    'wxiuytpolfyz.mp3': ['альвеолярный одноударный', 'ɾ'],
    'wxdftrhjipoyz.mp3': ['альвеолярный дрожащий', 'r'],
    'wxfdglkjhpoyz.mp3': ['билабиальный дрожащий', 'ʙ'],
    'wxpolkaserwtyz.mp3': ['лабиодентальный одноударный', 'ⱱ'],
    'wxdftrmlkiyz.mp3': ['ретрофлексный одноударный', 'ɽ'],
    'wxuinbfgtryz.mp3': ['увулярный дрожащий', 'ʀ'],
    'wxponbvfdcxyz.mp3': ['альвеолярный носовой', 'n'],
    'wxdfpolbvfyz.mp3': ['билабиальный носовой', 'm'],
    'wxnbvgfhuyz.mp3': ['лабиодентальный носовой', 'ɱ'],
    'wxcjhgfvcyz.mp3': ['палатальный носовой', 'ɲ'],
    'wxfdlkbvcfdyz.mp3': ['ретрофлексный носовой', 'ɳ'],
    'wxlkoimnbyz.mp3': ['увулярный носовой', 'ɴ'],
    'wxcvfgdsalyz.mp3': ['велярный носовой', 'ŋ'],
    'wxkjhgfvcnlkyz.mp3': ['гортанная смычка', 'ʔ'],
    'wxdsfapolkyz.mp3': ['звонкий увулярный взрывной', 'ɢ'],
    'wxdsakjhbvmnyz.mp3': ['звонкий велярный взрывной', 'g'],
    'wxfdsbvcdfreyz.mp3': ['глухой увулярный взрывной', 'q'],
    'wxcdsrewqpoyz.mp3': ['глухой велярный взрывной', 'k'],
    'wxfdgvcxlkyz.mp3': ['звонкий палатальный взрывной', 'ɟ'],
    'wxcfdgytuiyz.mp3': ['глухой палатальный взрывной', 'c'],
    'wxcfdtrlkopyz.mp3': ['звонкий ретрофлексный взрывной', 'ɖ'],
    'wxkjhgytryz.mp3': ['глухой ретрофлексный взрывной', 'ʈ'],
    'wxalkopvcyz.mp3': ['звонкий альвеолярный взрывной', 'd'],
    'wxertyczvgyz.mp3': ['глухой альвеолярный взрывной', 't'],
    'wxcfdretblkyz.mp3': ['звонкий билабиальный взрывной', 'b'],
    'wxczghftrlyz.mp3': ['глухой билабиальный взрывной', 'p'],
}
ipa_vowels = {
    'abhsjfgskcd.mp3': ['верхний подъём, задний ряд, огубленный', 'u'],
    'abdkjfgbjcd.mp3': ['верхний подъём, задний ряд, неогубленный', 'ɯ'],
    'abfghkjdhcd.mp3': ['верхний подъём, средний ряд, огубленный', 'ʉ'],
    'abgfhtrygkcd.mp3': ['верхний подъём, средний ряд, неогубленный', 'ɨ'],
    'abfythvncocd.mp3': ['верхний подъём, передний ряд, огубленный', 'y'],
    'abfungythpcd.mp3': ['верхний подъём, передний ряд, неогубленный', 'i'],
    'absdfrtyhccd.mp3': ['верхне-средний подъём, задний ряд, огубленный', 'o'],
    'abvfrtghcxcd.mp3': ['верхне-средний подъём, задний ряд, неогубленный', 'ɤ'],
    'abghtyubfcd.mp3': ['верхне-средний подъём, средний ряд, огубленный', 'ɵ'],
    'absdfrteyrcd.mp3': ['верхне-средний подъём, средний ряд, неогубленный', 'ɘ'],
    'aberdfghycd.mp3': ['верхне-средний подъём, передний ряд, огубленный', 'ø'],
    'abrtfdghyscd.mp3': ['верхне-средний подъём, передний ряд, неогубленный', 'e'],
    'ablkuitdfgtcd.mp3': ['средне-нижний подъём, передний ряд, неогубленный', 'ɛ'],
    'abttredcfhkcd.mp3': ['средне-нижний подъём, задний ряд, огубленный', 'ɔ'],
    'abfyuoqvbscd.mp3': ['средне-нижний подъём, задний ряд, неогубленный', 'ʌ'],
    'abzxyutrfgscd.mp3': ['средне-нижний подъём, средний ряд, огубленный', 'ɞ'],
    'abftrewrtyukcd.mp3': ['средне-нижний подъём, средний ряд, неогубленный', 'ɜ'],
    'abntyuretlogcd.mp3': ['средне-нижний подъём, передний ряд, огубленный', 'œ'],
    'abgtrewlpoixcd.mp3': ['ненапряжённый нижний, средний ряд, неогубленный', 'ɐ'],
    'abtyuevbnhgcd.mp3': ['ненапряжённый нижний, передний ряд, неогубленный', 'æ'],
    'abhytrfdjiokpcd.mp3': ['нижний подъём, передний ряд, неогубленный', 'a'],
    'abyuibvdfglmcd.mp3': ['нижний подъём, передний ряд, огубленный', 'ɶ'],
    'abhgfnmxcvfgcd.mp3': ['нижний подъём, задний ряд, неогубленный', 'ɑ'],
    'abtrenmjhyuxocd.mp3': ['нижний подъём, задний ряд, огубленный', 'ɒ'],
    'abwerxzsawhcd.mp3': ['средне-верхний подъём, ненапряжённый задний, огубленный', 'ʊ'],
    'abvcxloihbvxcd.mp3': ['средне-верхний подъём, ненапряжённый передний, огубленный', 'ʏ'],
    'abgtremlkoiucd.mp3': ['средне-верхний подъём, ненапряжённый передний, неогубленный', 'ɪ'],
    'abcxgtreyupocd.mp3': ['средний подъём, средний ряд, неогубленный', 'ə']
}
ipa_spectrograms = {
    'stuyvbcgftuv.png': 'dog',
    'stbvfgtyuiouv.png': 'duck',
    'stvcbxgftruv.png': 'pig',
    'stzxcghytruv.png': 'mouse',
    'strebvncjyv.png': 'horse',
    'strevbnjhyuv.png': 'fox',
    'stjkioptruv.png': 'deer',
    'strvcnhjupoiuv.png': 'sheep',
    'stcfgyuiouv.png': 'cow',
    'strefdcvxnuv.png': 'wolf',
    'strhfbcvxmuv.png': 'lion',
    'strevcbxnuv.png': 'tiger',
    'stbvcmxniouv.png': 'turkey',
    'stbvcmkjhiuv.png': 'parrot',
    'stgfhdjkionbuv.png': 'rabbit',
    'stvcxzmlkpouv.png': 'zebra',
    'stvcxbnmjhuv.png': 'koala',
    'stvcxbnhjlkuv.png': 'cheetah',
    'stgfvcbxnmiuv.png': 'leopard',
    'stvcbnjklgfuv.png': 'monkey',
    'stbvcmnhjklpxuv.png': 'squirrel',
    'stvcxnbjkluv.png': 'elephant',
    'stcvxbjhgflouv.png': 'hedgehog',
    'stvcxnhgfdlkouv.png': 'kangaroo',
}


# клавиатура для ответов (кроме спектрограмм, где ручной ввод)
answers_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
answers_markup.add(types.KeyboardButton('1'), types.KeyboardButton('2'), types.KeyboardButton('3'),
                   types.KeyboardButton('4'), types.KeyboardButton('5'), types.KeyboardButton('выйти из режима'))
right_answer = {}


def vowels_mode(message):
    global mode
    user_id = message.from_user.id
    mode[user_id] = 'vowels'
    # список из аудиофайлов для случайного выбора
    ids_vowels = list(ipa_vowels.keys())
    question = random.choice(ids_vowels)
    with open(question, 'rb') as audio:
        bot.send_audio(message.chat.id, audio, title='гласный')
    # выбор пяти случайных вариантов ответов из описаний
    descriptions = list(ipa_vowels.values())
    options = random.sample(descriptions, 5)
    # если среди ответов нет правильного, удаляем случайный и вставляем правильный
    if ipa_vowels[question] not in options:
        a = random.choice(options)
        options[options.index(a)] = ipa_vowels[question]
    random.shuffle(options)

    question_text = 'Как ты думаешь, что это?\n\n'

    number = 1
    for option in options:
        # выводим описание и символ через ';'
        option_output = '; '.join(option)
        # определяем номер правильного ответа (выводим в консоль для тестирования)
        if option == ipa_vowels[question]:
            global right_answer
            right_answer[user_id] = number
            print(right_answer)
        line = f'{number}) {option_output}\n'
        question_text += line
        number += 1

    bot.send_message(message.chat.id, text=f'{question_text}', reply_markup=answers_markup)


def consonants_mode(message):
    global mode
    user_id = message.from_user.id
    mode[user_id] = 'consonants'
    # список из аудиофайлов для случайного выбора
    ids_consonants = list(ipa_consonants.keys())
    question = random.choice(ids_consonants)
    with open(question, 'rb') as audio:
        bot.send_audio(message.chat.id, audio, title='согласный')
    # выбор пяти случайных вариантов ответов из описаний
    descriptions = list(ipa_consonants.values())
    options = random.sample(descriptions, 5)
    # если среди ответов нет правильного, удаляем случайный и вставляем правильный
    if ipa_consonants[question] not in options:
        a = random.choice(options)
        options[options.index(a)] = ipa_consonants[question]
    random.shuffle(options)

    question_text = 'Как ты думаешь, что это?\n\n'

    number = 1
    for option in options:
        # выводим описание и символ через ';'
        option_output = '; '.join(option)
        # определяем номер правильного ответа (выводим в консоль для тестирования)
        if option == ipa_consonants[question]:
            global right_answer
            right_answer[user_id] = number
            print(right_answer)
        line = f'{number}) {option_output} \n'
        question_text += line
        number += 1

    bot.send_message(message.chat.id, text=f'{question_text}', reply_markup=answers_markup)


def spectro_mode(message):
    global mode
    user_id = message.from_user.id
    mode[user_id] = 'spectro'
    # список изображений для случайного выбора
    ids_spectrograms = list(ipa_spectrograms.keys())
    question = random.choice(ids_spectrograms)
    with open(question, 'rb') as photo:
        bot.send_photo(message.chat.id, photo)
    # выбор пяти случайных вариантов ответов из описаний
    descriptions = list(ipa_spectrograms.values())
    options = random.sample(descriptions, 5)
    # если среди ответов нет правильного, удаляем случайный и вставляем правильный
    if ipa_spectrograms[question] not in options:
        a = random.choice(options)
        options[options.index(a)] = ipa_spectrograms[question]

    global right_answer
    right_answer[user_id] = ipa_spectrograms[question]
    print(right_answer)

    # клавиатура с цифрами не нужна т. к. в этом режиме ручной ввод ответа
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton('выйти из режима'))
    bot.send_message(message.chat.id, text='Как ты думаешь, какое это животное (англ.)?', reply_markup=markup)


def symbols_desc_mode(message):
    global mode
    user_id = message.from_user.id
    mode[user_id] = 'symbols_desc'
    # создаём словарь из пар символ — описание
    ipa_symbols_desc = {a[1]: a[0] for a in list(ipa_vowels.values()) + list(ipa_consonants.values())}
    # список символов для случайного выбора
    ids_symbols_desc = list(ipa_symbols_desc.keys())
    question = random.choice(ids_symbols_desc)
    # выбор пяти случайных вариантов ответа из описаний
    descriptions = list(ipa_symbols_desc.values())
    options = random.sample(descriptions, 5)
    # если среди ответов нет правильного, удаляем случайный и вставляем правильный
    if ipa_symbols_desc[question] not in options:
        a = random.choice(options)
        options[options.index(a)] = ipa_symbols_desc[question]
    random.shuffle(options)

    question_text = f'Как ты думаешь, что это?\n\n{question}\n\n'

    number = 1
    for option in options:
        question_text += f'{number}) {option}\n'
        # определяем номер правильного ответа (выводим в консоль для тестирования)
        if option == ipa_symbols_desc[question]:
            global right_answer
            right_answer[user_id] = number
            print(right_answer)
        number += 1

    bot.send_message(message.chat.id, text=f'{question_text}', reply_markup=answers_markup)


def symbols_audio_mode(message):
    global mode
    user_id = message.from_user.id
    mode[user_id] = 'symbols_audio'
    # создаём словарь из пар символ — аудио
    ipa_symbols_audio = dict((val[1], key) for key, val in (ipa_vowels | ipa_consonants).items())
    # список символов для случайного выбора
    ids_symbols_audio = list(ipa_symbols_audio.keys())
    question = random.choice(ids_symbols_audio)
    # выбор пяти случайных ответов из аудио
    audios = list(ipa_symbols_audio.values())
    options = random.sample(audios, 5)
    # если среди ответов нет правильного, удаляем случайный и вставляем правильный
    if ipa_symbols_audio[question] not in options:
        a = random.choice(options)
        options[options.index(a)] = ipa_symbols_audio[question]
    random.shuffle(options)

    bot.send_message(message.chat.id, text=f'Как ты думаешь, что это?\n\n{question}', reply_markup=answers_markup)

    number = 1
    for option in options:
        # отправка аудио для каждого варианта
        with open(option, 'rb') as audio:
            bot.send_audio(message.chat.id, audio, title=str(number), performer='вариант')
        # определение номера правильного ответа (и вывод в консоль для тестирования)
        if option == ipa_symbols_audio[question]:
            global right_answer
            right_answer[user_id] = number
            print(right_answer)
        number += 1


@bot.message_handler(content_types=['text'])
def check_answer(message):
    user_id = message.from_user.id
    if message.text == 'выйти из режима':
        restart(message)
    elif message.text.lower() == f'{right_answer[user_id]}':
        streak[user_id] += 1
        if streak[user_id] > max_streak[user_id]:
            max_streak[user_id] = streak[user_id]
        right_message = f'Правильно! Дёмдальш\n\nсерия ответов: ' + str(streak[user_id]) +\
                        '🔥' * (streak[user_id] // 5) + '\nваш рекорд: ' + str(max_streak[user_id])
        bot.send_message(message.chat.id, text=right_message)
        if mode[user_id] == 'vowels':
            vowels_mode(message)
        elif mode[user_id] == 'consonants':
            consonants_mode(message)
        elif mode[user_id] == 'spectro':
            spectro_mode(message)
        elif mode[user_id] == 'symbols_desc':
            symbols_desc_mode(message)
        elif mode[user_id] == 'symbols_audio':
            symbols_audio_mode(message)

    elif message.text.isdigit() or mode[user_id] == 'spectro':
        streak[user_id] = 0
        bot.send_message(message.chat.id, text='Неправильно! Попробуй ещё раз')


# Запускаем бот
bot.polling(none_stop=True, interval=0)
